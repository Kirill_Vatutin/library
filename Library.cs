﻿using System;
using System.Collections.Generic;

namespace Library
{
    class Library
    {
        List<Note> List_library = new List<Note>();
        public class Note
        {
            static private int id2 = 1;
            public int id { get; set; }//код
            public string name{ get; set; }
            protected int quantity;//количество
            protected int year;
            protected string publishing;//издательство
            public Note(string _name, int _quantity, int _year, string _publishing) //или лучше
                                                                                    //protected?

            {
                id = id2;
                id2++;
                name = _name;
                quantity = _quantity;
                year = _year;
                publishing = _publishing;
            }
            public Note()
            {

            }
            virtual public void print()
            {
                
                Console.WriteLine($"id:{id}");
                Console.WriteLine($"Название:{name}");
                Console.WriteLine($"Количество экземпляров:{quantity}");
                Console.WriteLine($"Год издание:{year}");
                Console.WriteLine($"Издательство:{publishing}");
            }
            public void print_author_and_id()
            { 
                Console.WriteLine($"id: {id}\nНазвание:{name}");
            }
           public virtual void Change_All(string _name, int _quantity, int _year, string _publishing) //или лучше
                                                                                    //protected?
            {
                name = _name;
                quantity = _quantity;
                year = _year;
                publishing = _publishing;
            }
        }//базовый класс
        class Book : Note
        {
            private string author;//автор
            private string genre;//жанр
            public Book(string _author, string _genre, string name, int quantity, int year, string publishing) :
                 base(name, quantity, year, publishing)
            {
                author = _author;
                genre = _genre;
            }

            public override void print()
            {

                Console.WriteLine($"id:{id}");
                Console.WriteLine($"Название:{name}");
                Console.WriteLine($"Количество экземпляров:{quantity}");
                Console.WriteLine($"Год издание:{year}");
                Console.WriteLine($"Издательство:{publishing}");
                Console.WriteLine($"Автор:{author}");
                Console.WriteLine($"Жанр:{genre}");
            }
            public override void Change_All(string _name, int _quantity, int _year, string _publishing) //или лучше
                                                                                                        //protected?
            {
                Console.WriteLine("Введите новое  автора: ");
                author = Console.ReadLine();//автор
                Console.WriteLine("Введите новое  жанр: ");
                genre = Console.ReadLine();//жанр
                name = _name;
                quantity = _quantity;
                year = _year;
                publishing = _publishing;
            }
        }//книги
        class Magazine : Note
        {
            private int periodicity;//переодичность
            private int number;//номер журнала
            public Magazine(int _periodicity, int _number, string name, int quantity, int year, string publishing) :
                 base(name, quantity, year, publishing)
            {
                periodicity = _periodicity;
                number = _number;
            }
            public override void print()
            {
              
                Console.WriteLine($"id:{id}");
                Console.WriteLine($"Название:{name}");
                Console.WriteLine($"Количество экземпляров:{quantity}");
                Console.WriteLine($"Год издание:{year}");
                Console.WriteLine($"Издательство:{publishing}");
                Console.WriteLine($"Переодичность выхода:{periodicity}");
                Console.WriteLine($"Номер журнала:{number}");
            }
            public override void Change_All(string _name, int _quantity, int _year, string _publishing) //или лучше
                                                                                                        //protected?
            {
                Console.WriteLine("Введите переодичность выхода журнала: ");
                periodicity = checking_year_or_quantity();//автор
                Console.WriteLine("Введите номер журнала: ");
                 number = checking_year_or_quantity();//жанр
                name = _name;
                quantity = _quantity;
                year = _year;
                publishing = _publishing;
            }

        }//журнал
        static  int checking_year_or_quantity()
        {
            int a;
            while (!(int.TryParse(Console.ReadLine(), out a)) || (a < 0))//здесь сделана скидка на то,
                                                                         //что книги могут быть только нашей эры,
                                                                         //то есть число неотрицательное,если нужно, я переделаю
            {
                Console.WriteLine("Введите положительное число!");
            }
            return a;
        }
        public void Add()
        {
            static int checking_number()
            {
                int a;
                while (!(int.TryParse(Console.ReadLine(), out a)) || (a > 1 || a < 0))
                {
                    Console.WriteLine("Допускается только 0-книга или 1-журнал !");
                }
                return a;
            }
           

            Console.WriteLine("Что вы хотите добавить в нашу библиотеку?0-книгу 1-журнал");
            int choise = checking_number();
            Note note = new Note();
            switch (choise)
            {
                case 0:
                    {
                        Console.WriteLine("Начнем добавление книги\nВведите название:");
                        string name = Console.ReadLine();
                        Console.WriteLine("Введите количество:");
                        int quantity = checking_year_or_quantity();//количество
                        Console.WriteLine("Введите год: ");
                        int year = checking_year_or_quantity();
                        Console.WriteLine("Введите издательсво: ");
                        string publishing = Console.ReadLine();//издательство
                        Console.WriteLine("Введите автора: ");
                        string author = Console.ReadLine();//автор
                        Console.WriteLine("Введите жанр: ");
                        string genre = Console.ReadLine();//жанр
                        note = new Book(author, genre, name, quantity, year, publishing);
                        break;
                    }
                case 1:
                    {
                        Console.WriteLine("Начнем добавление журнала\nВведите название:");
                        string name = Console.ReadLine();
                        Console.WriteLine("Введите количество:");
                        int quantity = checking_year_or_quantity();//количество
                        Console.WriteLine("Введите год: ");
                        int year = checking_year_or_quantity();
                        Console.WriteLine("Введите издательсво: ");
                        string publishing = Console.ReadLine();//издательство
                        Console.WriteLine("Введите переодичность выхода журнала: ");
                        int periodisity = checking_year_or_quantity();//автор
                        Console.WriteLine("Введите номер журнала: ");
                        int number = checking_year_or_quantity();//жанр
                        note = new Magazine(periodisity, number, name, quantity, year, publishing);

                        break;
                    }
            }
            List_library.Add(note);
            Console.WriteLine();




        }
        public void Delete()
        {
            foreach (var item in List_library)
            {
                item.print_author_and_id();
            }
            Console.Write("Введите id элемента, который нужно удалить ");
            int res = checking_year_or_quantity();
            List_library.RemoveAt(res-1);//отнимаем 1, так как начинается с единицы
            foreach (var item in List_library)//цикл нужен для обновления id-шек, так как они сместились
            {
                if (item.id > res) item.id--;
            }
        }
        public void Find()
        {
            Console.Write("Введите название книги:");
            string res = Console.ReadLine();
            Console.WriteLine();
            bool find = false;//проверка на то, нашлось ли хоть одно совпадение
            foreach (var item in List_library)
            {
                if (item.name == res)
                {
                    item.print_author_and_id();
                    find = true;
                }
                if (!find) Console.WriteLine("Такими книгами(журналами) пока не располагаем");
            }

        }
        public void Print_All_Collectoin_Library()
        {
            foreach (var item in List_library)
            {
                item.print();
            }
        }
        public void Change() 
        {
            static void Change_Base( Note item)//тут будем менять только общие члены у книг и журналов,
                                     //уникальные параметры меняются в функции Change_All
            {
                Console.WriteLine("Начнем изменение книги\nВведите новое название:");


                string name = Console.ReadLine();
                Console.WriteLine("Введите новое  количество:");
                int quantity = checking_year_or_quantity();//количество
                Console.WriteLine("Введите новое  год: ");
                int year = checking_year_or_quantity();
                Console.WriteLine("Введите новое  издательсво: ");
                string publishing = Console.ReadLine();//издательство
                item.Change_All(name, quantity, year, publishing);
            }
            foreach (var item in List_library)
            {
                item.print_author_and_id();
            }
            Console.Write("Введите id элемента, который нужно изменить");
            int res = checking_year_or_quantity();
            foreach (var item in List_library)
            {
                if (item.id == res)
                {
                Change_Base(item);           
                break;
                }
            }

        }
        static public void Print_Command()
        {
            Console.WriteLine("Добавить новый элемент-Add");
            Console.WriteLine("Удалить элемент-Delete");
            Console.WriteLine("Показать содержимое библиотеки-Print");
            Console.WriteLine("Изменить данные о книге(журнале)-Change");
        }
        
       

    }
}
