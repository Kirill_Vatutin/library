﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Thread1
{
    
    class ProgramThread1
    {
        static void SortArray(int[] array,int CountThread=1)
        {
            Task[] taskArray = new Task[CountThread];
            for (int i = 0; i < CountThread; i++)
            {
                int startIterator = array.Length / CountThread * i;
                int endIterator = array.Length / CountThread * (i + 1);
              //   taskArray[i] = Task.Factory.StartNew(() => BubbleSort(array, startIterator, endIterator));//из проблем. Это слишком долго,
              //   я даже дожаться не смог, но на чуть меньших числах все работает за адекватное количество времени, оно и понятно, сложность O(n^2)
                taskArray[i] = Task.Factory.StartNew(() => QuickSort(array, startIterator,endIterator-1));
            }
            Task.WaitAll(taskArray);
            PrintArray(array);
        }
        static void BubbleSort(int[] arr,int startiterator,int endIterator)
        {
            int temp;
            for (int i = startiterator; i < endIterator; i++)
            {
                for (int j = i + 1; j <endIterator; j++)
                {
                    if (arr[i] > arr[j])
                    {
                        temp = arr[i];
                        arr[i] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
           
        }
        static void QuickSort(int[] arr, int l, int r)
        {
            int i = l;
            int j = r;
            int x = arr[(l + r) / 2]; //Опорная

            while (i <= j)
            {

                while (arr[i] < x) i++;
                while (arr[j] > x) j--;
                //Если i<=j:
                if (i <= j)
                {
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                    i++;
                    j--;
                }
            }
            //Рекурсия
            if (l < j) QuickSort(arr, l, j);
            if (i < r) QuickSort(arr, i, r);
        }
        static void PrintArray(int []arr)
        {
            for (int i =240000; i < arr.Length; i++)
            {
                Console.WriteLine($"{i} {arr[i]}");
            }
        }
        
        static void EnterMassiv(int CountThread=1)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            int[] array = new int[1000000];
            Task[] taskArray = new Task[CountThread];
            for (int i = 0; i < CountThread; i++)
            {
                int startIterator = array.Length / CountThread * i;
                int endIterator = array.Length / CountThread * (i + 1) ;
               taskArray[i] = Task.Factory.StartNew(()=>
                {
                    for (int j = startIterator ; j <endIterator ; j++)
                    {
                        array[j] = new Random().Next()%10; 
                    }
                   
                }
                );
            }
            Task.WaitAll(taskArray);
            SortArray(array, CountThread);
            stopWatch.Stop();
            Console.WriteLine($"{stopWatch.ElapsedMilliseconds} миллисекунд потребовалось для заполнения массива в {CountThread} потоках");
           
           

        }
        static void Main()
        {
           
           EnterMassiv(1);
           EnterMassiv(2);
           EnterMassiv(4);
        }

    }
}