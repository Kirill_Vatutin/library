﻿using System;
using Library;

using static Library.Library;

namespace Library
{
    class Program
    {
        static  void interaction()
        {
            Library library = new Library();
            string choise = "";
            Console.WriteLine("Введите название комманды.Для вывода списка комманд введите -help");
            while (choise!="-end")
            {
                Console.Write("Введите название комманды: ");
                choise = Console.ReadLine().ToLower();//перевод к нижнему для удобства пользователя
                
                switch (choise)
                {
                    case "-help":
                        {
                            Library.Print_Command();
                            break;
                        }
                    case "-end":
                        {

                            break;
                        }
                    case "add":
                        {
                            library.Add();
                            break;
                        }
                    case "delete":
                        {
                            library.Delete();
                            break;
                        }
                    case "print":
                        {
                            library.Print_All_Collectoin_Library();
                            break;
                        }
                    case "find":
                        {
                            library.Find();
                            break;
                        }
                    case "change":
                        {
                            library.Change();
                            break;
                        }
                
                    default:
                        {
                            Console.WriteLine("Указано неправильное имя комманды. Для вывода списка комманд введите -help");
                            break;
                        }
                }
            }
        }
        static void Main(string[] args)
        {
            interaction();
           
           
        }
       
    }
}
